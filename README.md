# Script de création de compte wireguard

## Installation

Clonez ce repo, puis

```bash
cd Baionet-Wireguard-Config && sudo make install
```

Si c'est la première fois que vous installez le script, un fichier de
configuration example sera crée à `/etc/wireguard/wg-create-account.ini`.

Il vous faudra le compléter en fonction de votre setup.

## Utilisation

```bash
sudo create_new_wireguard_account
```

Pour créer interactivement un nouveau compte.
