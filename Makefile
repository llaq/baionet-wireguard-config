.POSIX:

DIST = /usr/sbin
CONF = /etc/wireguard

install:
	cp -f create_new_wireguard_account $(DIST)
	chmod 100 $(DIST)/create_new_wireguard_account
	if [ ! -f $(CONF)/wg-create-account.ini ]; \
		then cp wg-create-account.ini.sample $(CONF)/wg-create-account.ini && \
		echo "Conf file sample created at $(CONF)/wg-create-account.ini; please fill it up."; \
	fi;

uninstall:
	rm -f $(DIST)/create_new_wireguard_account

.PHONY: install uninstall
